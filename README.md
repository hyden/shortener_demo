# Shortener Demo Django Project #

Shortener project written in Django 1.10.

## How to Use

To use this project, follow these steps:

1. Clone the repo `$ git clone https://hyden@bitbucket.org/hyden/shortener_demo.git shortener_bartoszukpiotr`
2. `$ cd shortener_bartoszukpiotr`
3. `$ pip install -r requirements.txt`
4. `$ python manage.py migrate`
5. `$ python manage.py create_fake_users 100`
6. `$ python manage.py runserver`
7. Open localhost:8000

## Python

Project was tested with Python 3.6.0

## Live demo

There should be a live demo at [http://still-island-18153.herokuapp.com/](http://still-island-18153.herokuapp.com/)