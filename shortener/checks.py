
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.checks import Error
from django.core.checks import register
from django.db import OperationalError


@register()
def shortener_settings_check(app_configs, **kwargs):
    errors = []
    try:
        assert isinstance(settings.SHORTENER_URL_KEY_LENGTH_BOUNDS, tuple)
        len_from, len_to = map(int, settings.SHORTENER_URL_KEY_LENGTH_BOUNDS)
        assert len_from <= len_to
        assert len_to <= 64  # length of db field
    except (AttributeError, ValueError, AssertionError):
        errors.append(
            Error(
                'SHORTENER_URL_KEY_LENGTH_BOUND setting is incorrect',
                hint='SHORTENER_URL_KEY_LENGTH_BOUND setting should be a tuple with min and max length, eg. (3, 8). '
                     'Max length should be lower than 64.',
                obj=settings,
                id='shortener.E001',
            )
        )
    try:
        if hasattr(settings, 'SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS'):
            assert isinstance(settings.SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS, int)
            assert settings.SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS >= 0
    except AssertionError:
        errors.append(
            Error(
                'SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS setting is incorrect',
                hint='SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS setting should be nonnegative integer',
                obj=settings,
                id='shortener.E002',
            )
        )
    try:
        number_of_users = get_user_model().objects.count()
        assert number_of_users > 0
    except OperationalError:
        pass  # there is no user table yet
    except AssertionError:
        errors.append(
            Error(
                'There should be at least one user in database',
                hint='Use python manage.py create_fake_users <number> to generate some users',
                obj=settings,
                id='shortener.E003',
            )
        )
    return errors
