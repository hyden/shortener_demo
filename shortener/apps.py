from django.apps import AppConfig
import shortener.checks


class ShortenerConfig(AppConfig):
    name = 'shortener'
