import requests
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from requests import RequestException


class Command(BaseCommand):
    help = 'Creates the specified number of fake users'
    requires_system_checks = False

    def add_arguments(self, parser):
        parser.add_argument('number_of_users', nargs=1, type=int)

    def handle(self, *args, **options):
        n = options['number_of_users'][0]
        request_params = {
            'inc': 'login,name,email,registered',
            'results': str(n),
            'noinfo': '',
        }

        try:
            r = requests.get('https://randomuser.me/api/', params=request_params)
        except RequestException:
            raise CommandError('API connection error occurred')

        json_users = r.json()['results']
        for json_user in json_users:
            get_user_model().objects.create(
                username=json_user['login']['username'],
                first_name=json_user['name']['first'],
                last_name=json_user['name']['last'],
                email=json_user['email'],
                password=json_user['login']['password'],
                date_joined=json_user['registered']
            )

        self.stdout.write(self.style.SUCCESS('Successfully created %s users' % n))
