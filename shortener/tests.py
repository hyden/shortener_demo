import re
from unittest import mock

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.db.utils import IntegrityError
from django.test import TestCase
from django.test import override_settings
from django.urls import reverse
import status

import shortener.utils
from shortener.models import Link


class LinkUrlKeyGeneratingTests(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create(username='test', email='test@example.com', password='password')

    def test_key_is_generated(self):
        """
        Tests if unique key is generated for every link
        """
        link_a = Link.objects.create(redirection_url='a', submitter=self.user)
        link_b = Link.objects.create(redirection_url='b', submitter=self.user)
        self.assertNotEqual(link_a.url_key, link_b.url_key)

    @mock.patch('shortener.utils.random_key', lambda x: 'a')
    def test_key_collision_raises_integrity_error(self):
        """
        Generating always the same url_key causes IntegrityError
        """
        with self.assertRaises(IntegrityError):
            Link.objects.create(redirection_url='a', submitter=self.user)
            Link.objects.create(redirection_url='b', submitter=self.user)

    @mock.patch('shortener.utils.random_key', mock.Mock(side_effect=['A', 'A', 'A', 'B']))
    def test_attempts_to_generate_url_key_until_gets_correct_key(self):
        """
        In case of key collision should reattempts to generate a key
        """
        first_link = Link.objects.create(redirection_url='a', submitter=self.user)
        second_link = Link.objects.create(redirection_url='b', submitter=self.user)
        self.assertEqual(first_link.url_key, 'A')
        self.assertEqual(second_link.url_key, 'B')

    @override_settings(SHORTENER_URL_KEY_LENGTH_BOUNDS=(2, 4))
    def test_url_key_has_correct_length(self):
        """
        Returned key's length should be equal to min_length from settings
        """
        link = Link.objects.create(redirection_url='a', submitter=self.user)
        self.assertEqual(len(link.url_key), 2)

    @override_settings(SHORTENER_URL_KEY_LENGTH_BOUNDS=(2, 4))
    def test_url_key_length_increases_up_to_max_value(self):
        """
        Returned key's length should increase up to max_length from settings
        """
        self.assertEqual(len(shortener.utils.random_key(attempt=0)), 2)
        self.assertEqual(len(shortener.utils.random_key(attempt=1)), 3)
        self.assertEqual(len(shortener.utils.random_key(attempt=2)), 4)
        self.assertEqual(len(shortener.utils.random_key(attempt=3)), 4)  # still 4


class ShortenerRedirectionTests(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create(username='test', email='test@example.com', password='password')

    def test_link_redirection_works(self):
        """
        Should redirect to redirection_url with 302 http status code
        """
        link = Link.objects.create(redirection_url='http://example.com/', submitter=self.user)
        response = self.client.get(reverse('shortener:redirect_to_url', args=(link.url_key,)))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, 'http://example.com/')

    def test_link_counter_increases_on_redirect(self):
        """
        Link counter increases by 1 after every redirection
        """
        link = Link.objects.create(redirection_url='http://example.com/', submitter=self.user)
        self.assertEqual(link.counter, 0)
        self.client.get(reverse('shortener:redirect_to_url', args=(link.url_key,)))
        self.assertEqual(Link.objects.get(id=link.id).counter, 1)
        self.client.get(reverse('shortener:redirect_to_url', args=(link.url_key,)))
        self.assertEqual(Link.objects.get(id=link.id).counter, 2)


class LinksAddingTests(TestCase):

    def setUp(self):
        self.url_key_pattern = re.compile('/!(.*)/')  # detail url path part pattern eg. /!a5teFb/
        self.user = get_user_model().objects.create(username='test', email='test@example.com', password='password')

    def _add_link(self, url):
        """
        Adds new link by sending request.
        :param url: redirection url
        :return: url_key or None if not added
        """
        payload = {'redirection_url': url}
        response = self.client.post(reverse('shortener:index'), data=payload)
        if response.status_code == status.HTTP_302_FOUND:
            match = self.url_key_pattern.search(response.url)
            return match.group(1)
        else:
            return None

    def test_add_new_link(self):
        """
        Should add new link
        """
        payload = {'redirection_url': 'http://example.com/'}
        response = self.client.post(reverse('shortener:index'), data=payload)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        match = self.url_key_pattern.search(response.url)
        self.assertIsNotNone(match)
        link = Link.objects.get(url_key=match.group(1))
        self.assertEqual(link.redirection_url, 'http://example.com/')

    def test_adding_same_url_twice(self):
        """
        Should not create another link for the same url
        """
        first_key = self._add_link('http://example.com/')
        second_key = self._add_link('http://example.com/')
        self.assertIsNotNone(first_key)
        self.assertEqual(first_key, second_key)

    def test_redirection_url_without_protocol(self):
        """
        Should add http prefix when needed
        """
        url_key = self._add_link('example.com/')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/')

    def test_redirection_url_with_https_protocol(self):
        """
        Should ignore https protocol
        """
        url_key = self._add_link('https://example.com/')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'https://example.com/')

    def test_redirection_url_without_trailing_slash(self):
        """
        Should add trailing slash
        """
        url_key = self._add_link('http://example.com')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/')

    def test_redirection_url_with_special_characters(self):
        """
        Should encode special characters
        """
        url_key = self._add_link('http://example.com/^"')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/%5E%22')

    def test_redirection_url_with_already_encoded_characters(self):
        """
        Should ignore already encoded characters
        """
        url_key = self._add_link('http://example.com/%5E%22%3C%3E')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/%5E%22%3C%3E')

    def test_redirection_url_with_both_encoded_and_not_encoded_characters(self):
        """
        Should encode only not encoded characters
        """
        url_key = self._add_link('http://example.com/abc %20')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/abc%20%20')

    def test_redirection_url_with_uppercase_domain(self):
        """
        Should lowercase domain
        """
        url_key = self._add_link('http://EXAMple.com/')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/')

    def test_redirection_url_with_uppercase_path_part(self):
        """
        Should ignore uppercase in path part
        """
        url_key = self._add_link('http://example.com/abcDEF')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/abcDEF')

    def test_redirection_url_with_get_parameters(self):
        """
        Should ignore get parameters
        """
        url_key = self._add_link('http://example.com/?q=abc&b=DEF')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.com/?q=abc&b=DEF')

    def test_incorrect_redirection_url(self):
        """
        Should not add incorrect url
        """
        url_key = self._add_link('http://example/')
        self.assertIsNone(url_key)  # not added

    def test_redirection_url_with_javascript_snippet(self):
        """
        Should not add javascipt snippet
        """
        url_key = self._add_link('javascript:(function()%7Balert(%22test!%22)%7D)()')
        self.assertIsNone(url_key)

    def test_redirection_url_with_ftp_protocol(self):
        """
        Should not add ftp protocol url
        """
        url_key = self._add_link('ftp://example.com/')
        self.assertIsNone(url_key)  # not added

    def test_redirection_url_with_cricket_domain(self):
        """
        Should add redirection url with new domain like .cricket
        """
        url_key = self._add_link('http://example.cricket/')
        link = Link.objects.get(url_key=url_key)
        self.assertEqual(link.redirection_url, 'http://example.cricket/')


class CreateFakeUsersTest(TestCase):

    def test_creating_one_fake_users(self):
        count_before = get_user_model().objects.count()
        call_command('create_fake_users', '1')
        count_after = get_user_model().objects.count()

        self.assertEqual(count_after - count_before, 1)

    def test_creating_ten_fake_users(self):
        count_before = get_user_model().objects.count()
        call_command('create_fake_users', '10')
        count_after = get_user_model().objects.count()

        self.assertEqual(count_after - count_before, 10)
