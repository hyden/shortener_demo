from django.conf import settings
from django.db import models

import shortener.utils


def generate_key():
    i = 0
    while i < getattr(settings, 'SHORTENER_URL_KEY_MAX_GENERATION_ATTEMPTS', 16):
        key = shortener.utils.random_key(i)
        try:
            Link.objects.get(url_key=key)
        except Link.DoesNotExist:
            return key
        i += 1
    return shortener.utils.random_key(i)  # may cause IntegrityError


class Link(models.Model):
    url_key = models.CharField(max_length=64, unique=True, default=generate_key)
    redirection_url = models.CharField(max_length=512, unique=True)
    counter = models.IntegerField(default=0)
    submitter = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.redirection_url
