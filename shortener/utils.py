import random

from django.conf import settings


def random_key(attempt=0):
    min_length, max_length = settings.SHORTENER_URL_KEY_LENGTH_BOUNDS
    key_length = min(min_length + attempt, max_length)  # key length increases with attempt number up to max_length
    alphabet = "abcdefghijkmnoprstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789"  # without some misleading chars like l and 0
    return "".join(random.choice(alphabet) for _ in range(key_length))
