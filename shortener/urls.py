from django.conf.urls import url

from . import views

app_name = 'shortener'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<url_key>[A-Za-z0-9]+)/$', views.redirect_to_link, name='redirect_to_url'),
    url(r'^!(?P<url_key>[A-Za-z0-9]+)/$', views.detail, name='detail'),
]