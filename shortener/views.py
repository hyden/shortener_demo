import random

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from url_normalize import url_normalize

from shortener.models import Link


def index(request):
    try:
        redirection_url = request.POST['redirection_url']
    except KeyError:
        return render(request, 'shortener/index.html')

    normalized_url = url_normalize(redirection_url)
    try:
        validator = URLValidator(schemes=['http', 'https'])
        validator(normalized_url)
    except ValidationError:
        return render(request, 'shortener/index.html', {'error_message': 'Submitted URL is invalid.'})

    user_count = get_user_model().objects.count()
    random_user = get_user_model().objects.all()[random.randrange(user_count)]
    link, create = Link.objects.get_or_create(redirection_url=normalized_url, defaults={'submitter': random_user})
    return HttpResponseRedirect(reverse('shortener:detail', args=(link.url_key,)))


def detail(request, url_key):
    link = get_object_or_404(Link, url_key=url_key)
    return render(request, 'shortener/detail.html', {'link': link})


def redirect_to_link(request, url_key):
    link = get_object_or_404(Link, url_key=url_key)
    link.counter = F('counter') + 1  # avoids race condition
    link.save()
    return HttpResponseRedirect(link.redirection_url)
